const projects = Array.from(document.getElementsByClassName('project-tile'));

projects.forEach(project => {
  project.addEventListener("mouseenter", handleMouseEnter);
  project.addEventListener("mouseleave", handleMouseLeave);
});


function handleMouseEnter(event) {
  projects.forEach(project => {
    if (event.target.id !== project.id) {
      setProjectTileBrightness(project, 50);
    }
  });

  setProjectImageGrayscale(event.target.id, 0);
}

function handleMouseLeave() {
  projects.forEach(project => {
    setProjectTileBrightness(project, 100);
    setProjectImageGrayscale(project.id, 1);
  });
}

function setProjectTileBrightness(project, value) {
  project.style.filter = `brightness(${value}%)`;
}

function setProjectImageGrayscale(projectId, value) {
  const img = document.getElementById(`image-${projectId}`);
  if (img) img.style.filter = `grayscale(${value})`;
}